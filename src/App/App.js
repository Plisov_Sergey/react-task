import React, { Component } from 'react';
import './App.css';
import Header from '../Header/Header';
import Viewer from '../Viewer/Viewer';
import Paginator from '../Paginator/Paginator';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
     searchResponse: {},
     selectedPage: 1,
     title: '',
    }
    this.fetchData=this.fetchData.bind(this);
    this.handlePageChange=this.handlePageChange.bind(this);
    this.handleTitleChange=this.handleTitleChange.bind(this);
  }
  fetchData() {
    const {selectedPage, title} = this.state;
    fetch(`https://www.omdbapi.com/?i=tt3896198&apikey=8523cbb8&s=${title}&page=${selectedPage}`)
      .then(response => response.json())
      .then(response =>{
        const searchResponse = response.Search ? response : {};
        this.setState({
          searchResponse, 
        })
      } )
  }

  handlePageChange(page, totalPage) {
    if (page < 1 || page > totalPage ) return;
    this.setState({
      selectedPage: page,
    }, this.fetchData)
  }
  handleTitleChange(e) {
    this.setState({
      title: e.target.value,
    }, this.fetchData)
  }

  render() {
    const {title, selectedPage, searchResponse} = this.state;
    return (
      <div>
        <Header handleTitleChange={this.handleTitleChange} title={title} />
        {searchResponse.Search && <Viewer  title={title} searchResponse={searchResponse} />}
        {searchResponse.Search && <Paginator
          selectedPage={selectedPage}
          totalResults={searchResponse.totalResults}
          handlePageChange={this.handlePageChange}  />}
      </div>
     
    );
  }
}

export default App;

import React from 'react';
import './Header.css';

const Header = ({title, handleTitleChange}) => (
  <div className="header">
        <h1>Movie Catalog</h1>
        <input type="text" className="input-value" value={title} onChange={handleTitleChange} />
        <select>
          <option>Sergey Plisov</option>
        </select>
  </div>
);
export default Header


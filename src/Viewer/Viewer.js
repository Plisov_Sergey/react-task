import React from 'react';
import Movie from '../Movie/Movie'
import './Viewer.css';

export default class Viewer extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        page: 0
      }
    }
    render() {
      const {searchResponse, title} = this.props;
      return (
          <div>
            <h3>You searched for: {title}, {searchResponse.totalResults} results found</h3>
            <div className="wraper-movie">
              {searchResponse.Search.map(item => {
                return <Movie {...item} />
              })}
            </div>
          </div>
          )
      }
  }
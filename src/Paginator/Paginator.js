import React from 'react';
import './Paginator.css';

const Paginator = ({selectedPage, totalResults, handlePageChange}) => {
  const totalPages = Math.ceil(totalResults/10);
  const pages = [...Array.from(new Array(totalPages).keys()).slice(1), totalPages];
  return (
    <div className="wraper-paginator">
      <span onClick={() => handlePageChange(selectedPage-1, totalPages)}>&lt;</span>
      {pages.map((page) => {
        let cl = 'page';
        if ( page ===  selectedPage ) cl += ' selected';
        const handleClick = () => handlePageChange(page, totalPages);
        return (
          <span className={cl} onClick={handleClick}>{page}</span>
        )
      
      })}
      <span onClick={() => handlePageChange(selectedPage+1, totalPages)}>&gt;</span>
    </div>
    
  );
};

export default Paginator;
import React from 'react';
import './Movie.css';

const Movie = ({Title, Year, Type, imdbID, Poster}) => (
    <div className="card" key={Title+Year}>
        <img className="card-image" src={Poster} title="pic"></img>
        <div className="wraper-info">
            <p>Name: {Title}</p>
            <p>Year: {Year}</p>
            <p>imdbID: {imdbID}</p>
            <p>Type: {Type}</p>
        </div>
      
    </div>
);
export default Movie;

